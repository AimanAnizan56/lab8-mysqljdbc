package com.csclab.mysqljdbc;

public interface MyVehicle {
    public final String MyVehicleType = "Car";
    public final String MyVehicleBrand = "Proton Exora";
    public final String MyVehicleID = "AIM1056";
}
