package com.csclab.mysqljdbc;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MyselfServlet", value = "/MyselfServlet")
public class MyselfServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        final String myName = (String) session.getAttribute("myName"),
                tempAge =  (String) session.getAttribute("myAge"),
                myHobbies = (String) session.getAttribute("myHobbies");

        if(tempAge == null || myName == null || myHobbies == null) {
            return;
        }

        int myAge = Integer.parseInt(tempAge);

        MySelf myself = new MySelf(myName, myAge, myHobbies);

        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' href='style.css'>");
        out.println("</head>");
        out.println("<body>");

        out.println("<ul class='listbox'>");
        out.println(myself.PrintInfo());
        out.println("</ul>");

        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
