package com.csclab.mysqljdbc;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MyStudentServlet", value = "/MyStudentServlet")
public class MyStudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        final String myName = (String) session.getAttribute("myName"),
                tempAge =  (String) session.getAttribute("myAge"),
                myHobbies = (String) session.getAttribute("myHobbies"),
                myMatricNo = (String) session.getAttribute("myMatricNo"),
                myProgramCode = (String) session.getAttribute("myProgramCode"),
                myCampus = (String) session.getAttribute("myCampus");

        if(tempAge == null || myName == null || myHobbies == null
                || myMatricNo == null || myProgramCode == null || myCampus == null) {
            return;
        }

        int myAge = Integer.parseInt(tempAge);

        MyStudent student = new MyStudent(myName,myAge,myHobbies,myMatricNo,myProgramCode,myCampus);

        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' href='style.css'>");
        out.println("</head>");
        out.println("<body>");

        out.println("<ul class='listbox'>");
        out.println(student.PrintInfo());
        out.println("</ul>");

        out.println("</body></html>");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
