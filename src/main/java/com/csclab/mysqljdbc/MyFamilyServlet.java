package com.csclab.mysqljdbc;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MyFamilyServlet", value = "/MyFamilyServlet")
public class MyFamilyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        final String myName = (String) session.getAttribute("myName"),
                tempAge =  (String) session.getAttribute("myAge"),
                myHobbies = (String) session.getAttribute("myHobbies"),
                myDad = (String) session.getAttribute("myDad"),
                myMom = (String) session.getAttribute("myMom"),
                mySiblings = (String) session.getAttribute("mySiblings");

        if(tempAge == null || myName == null || myHobbies == null
                || myDad == null || myMom == null || mySiblings == null) {
            return;
        }

        int myAge = Integer.parseInt(tempAge);

        MyFamily family = new MyFamily(myName, myAge, myHobbies, myDad, myMom, mySiblings);

        out.println("<html>");
        out.println("<head>");
        out.println("<link rel='stylesheet' href='style.css'>");
        out.println("</head>");
        out.println("<body>");

        out.println("<ul class='listbox'>");
        out.println(family.PrintInfo());
        out.println("</ul>");

        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
