package com.csclab.mysqljdbc;

import com.csclab.connection.Mysql;
import org.json.JSONObject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.PreparedStatement;

@WebServlet(name = "JdbcServlet", value = "/JdbcServlet")
public class JdbcServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.getWriter().println(new JSONObject().put("message", "nothing here"));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String myName, tempMyAge, myHobbies, myDad, myMom, mySiblings, myMatricNo, myProgramCode, myCampus;
        response.setContentType("application/json");

        BufferedReader br = request.getReader();
        JSONObject jsonObj = null;
        if (br.ready()) {
            jsonObj = new JSONObject(br.readLine());
        }
        br.close();

        myName = jsonObj.getString("myName");
        tempMyAge = jsonObj.getString("myAge");
        myHobbies = jsonObj.getString("myHobbies");
        myDad = jsonObj.getString("myDad");
        myMom = jsonObj.getString("myMom");
        mySiblings = jsonObj.getString("mySiblings");
        myMatricNo = jsonObj.getString("myMatricNo");
        myProgramCode = jsonObj.getString("myProgramCode");
        myCampus = jsonObj.getString("myCampus");

        if(checkNull(new String[]{myName, tempMyAge, myHobbies, myDad, myMom, mySiblings, myMatricNo, myProgramCode, myCampus})){
            response.setStatus(response.getStatus());
            response.getWriter().println(new JSONObject().put("error","Null value given!"));
        };
        int affectedRow = 0;

        try {
            int myAge = Integer.parseInt(tempMyAge);
            String sql = "INSERT INTO myself (myName, myAge, myHobbies, myDad, myMom, mySiblings, myMatricNo, myProgramCode, myCampus) VALUES (?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = Mysql.getConnection().prepareStatement(sql);

            stmt.setString(1, myName);
            stmt.setInt(2, myAge);
            stmt.setString(3, myHobbies);
            stmt.setString(4, myDad);
            stmt.setString(5, myMom);
            stmt.setString(6, mySiblings);
            stmt.setString(7, myMatricNo);
            stmt.setString(8, myProgramCode);
            stmt.setString(9, myCampus);

            affectedRow = stmt.executeUpdate();
            Mysql.closeConnection();
        } catch (Exception err) {
            response.setStatus(response.getStatus());
            response.getWriter().println(new JSONObject().put("error",err.getMessage()));
            err.printStackTrace();
        }

        JSONObject json = new JSONObject();
        if(affectedRow == 1) {
            json.put("affected_row", affectedRow);
        } else {
            json.put("affected_row", affectedRow);
            response.setStatus(400);
        }

        response.getWriter().println(json);
    }

    public static boolean checkNull(String []data) {
        final int SIZE = data.length;
        for(int i = 0; i < SIZE; i++) {
            if(data[i] == null || data[i].equals("")) {
                return true;
            }
        }
        return false;
    }
}
