package com.csclab.connection;

import java.sql.*;

public abstract class Mysql {
    private static Connection conn = null;
    private static void initDB() throws SQLException, ClassNotFoundException{
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/myinfo","root","");
    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if(conn == null || conn.isClosed()) {
            initDB();
        }
        return conn;
    }

    public static void closeConnection() throws SQLException{
        if(!conn.isClosed() || conn != null) {
            conn.close();
            conn = null;
        }
    }
}
