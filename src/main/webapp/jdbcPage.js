window.addEventListener("DOMContentLoaded", () => {
    let form = document.getElementsByTagName("form");
    let modalBtn = document.querySelector(".modal > .modal-btn > .btn-close");
    modalBtn.addEventListener('click',closeModal);
    for(i=0; i<form.length; i++) {
        form[i].addEventListener('submit',formHandler)
    }
})

function closeModal(event) {
    event.preventDefault();
    const modal = event.target.parentNode.parentNode;
    modal.classList.remove("open");
}

function formHandler(event) {
    event.preventDefault();
    let formObj = event.target;
    let formData = new FormData(formObj);
    let inputData = {};

    for(let key of formData.keys()) {
        inputData[key] = formData.get(key);
    }

    axios({
        url: 'JdbcServlet',
        method: 'post',
        data: inputData
    })
    .then(function(response) {
        const modal = document.querySelector("#modalbox");
        const modalTitle = document.querySelector("#modalbox > .modal-title")
        const modalDesc = document.querySelector("#modalbox > .modal-desc")
        if(response.data['affected_row'] === 1) {
            modalTitle.innerText = "Succeed";
            modalDesc.innerText = "The information has been added into database.";
            modal.classList.add("open");
        } else {
            modalTitle.innerText = "Failed";
            modalDesc.innerText = "Cannot add information into database.";
            modal.classList.add("open");
        }
    })
    .catch(function(error) {
        console.log(error, error.value)
    })
    .finally(function() {
        formObj.reset();
    })
}