<%--
  Created by IntelliJ IDEA.
  User: Aiman
  Date: 22/12/2021
  Time: 3:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>JSTL Pages</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <%@include file="Menu.html"%>
    <div class="container">
        <table>
            <tr>
                <th>Counter(Age)</th>
                <th>Output</th>
            </tr>
            <c:if test="${myAge <= 14}">
                <c:forEach var="counter" begin="1" end="${myAge}">
                    <tr>
                        <td><c:out value="${counter}"/></td>
                        <td><c:out value="${myName}, you are in childhood age"/></td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${myAge >= 15 && myAge <= 24}">
                <c:forEach var="counter" begin="1" end="${myAge}">
                    <tr>
                        <td><c:out value="${counter}"/></td>
                        <td><c:out value="${myName}, you are in youth age"/></td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${myAge >= 25 && myAge <= 64}">
                <c:forEach var="counter" begin="1" end="${myAge}">
                    <tr>
                        <td><c:out value="${counter}"/></td>
                        <td><c:out value="${myName}, you are in adulthood age"/></td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${myAge >= 65}">
                <c:forEach var="counter" begin="1" end="${myAge}">
                    <tr>
                        <td><c:out value="${counter}"/></td>
                        <td><c:out value="${myName}, you are in seniority age"/></td>
                    </tr>
                </c:forEach>
            </c:if>
        </table>
        <a href="./">
            <button>Back</button>
        </a>
    </div>
</body>
</html>
