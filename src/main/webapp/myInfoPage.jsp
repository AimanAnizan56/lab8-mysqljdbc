<%--
  Created by IntelliJ IDEA.
  User: Aiman
  Date: 15/1/2022
  Time: 2:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>myInfo Database</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <%@include file="Menu.html"%>
    <sql:setDataSource var="conn" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost/myinfo" user="root" password=""/>

    <sql:query var="result" dataSource="${conn}">
        SELECT * FROM myself;
    </sql:query>

    <div class="container">
        <h1>All data in myself table</h1>
        <table>
            <tr>
                <th>No</th>
                <th>myName</th>
                <th>myAge</th>
                <th>myHobbies</th>
                <th>myMatricNo</th>
                <th>myProgramCode</th>
                <th>myCampus</th>
                <th>myDad</th>
                <th>myMom</th>
                <th>mySiblings</th>
            </tr>

            <c:choose>
                <c:when test="${result.rowCount == 0}">
                    <tr><td colspan="10" style="text-align: center">Nothing in database</td></tr>
                </c:when>
                <c:otherwise>
                    <c:set var="count" value="1"/>
                    <c:forEach var="row" items="${result.rows}">
                        <tr>
                            <td> <c:out value="${count}"/> </td>
                            <td> <c:out value="${row.myName}"/> </td>
                            <td> <c:out value="${row.myAge}"/> </td>
                            <td> <c:out value="${row.myHobbies}"/> </td>
                            <td> <c:out value="${row.myMatricNo}"/> </td>
                            <td> <c:out value="${row.myProgramCode}"/> </td>
                            <td> <c:out value="${row.myCampus}"/> </td>
                            <td> <c:out value="${row.myDad}"/> </td>
                            <td> <c:out value="${row.myMom}"/> </td>
                            <td> <c:out value="${row.mySiblings}"/> </td>
                        </tr>
                        <c:set var="count" value="${count + 1}"/>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </table>
        <a href="jdbcPage.jsp"><button>Back</button></a>
    </div>
</body>
</html>
