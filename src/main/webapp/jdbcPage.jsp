<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JDBC Page - Input to the DB</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <%@include file="Menu.html"%>
    <div class="container">
        <h1>Enter input below to insert into DB</h1>
        <form id="allform" name="allform" method="post">
            <div>
                <label for="myName">Name</label>
                <input type="text" name="myName" id="myName" autocomplete="off" required>
            </div>
            <div>
                <label for="myAge">Age</label>
                <input type="number" name="myAge" id="myAge" autocomplete="off" required>
            </div>
            <div>
                <label for="myHobbies">Hobbies</label>
                <input type="text" name="myHobbies" id="myHobbies" autocomplete="off" required>
            </div>
            <div>
                <label for="myDad">Dad</label>
                <input type="text" name="myDad" id="myDad" autocomplete="off" required>
            </div>
            <div>
                <label for="myMom">Mom</label>
                <input type="text" name="myMom" id="myMom" autocomplete="off" required>
            </div>
            <div>
                <label for="mySiblings">Siblings</label>
                <input type="text" name="mySiblings" id="mySiblings" autocomplete="off" required>
            </div>
            <div>
                <label for="myMatricNo">Matric No</label>
                <input type="text" name="myMatricNo" id="myMatricNo" autocomplete="off" required>
            </div>
            <div>
                <label for="myProgramCode">Program Code</label>
                <input type="text" name="myProgramCode" id="myProgramCode" autocomplete="off" required>
            </div>
            <div>
                <label for="myCampus">Campus</label>
                <input type="text" name="myCampus" id="myCampus" autocomplete="off" required>
            </div>
            <div>
                <button type="submit">Submit</button>
            </div>
        </form>
        <a href="myInfoPage.jsp">
            Click here to see all value in myinfo
        </a>
    </div>
    <div id="modalbox" class="modal">
        <div class="modal-title"> </div>
        <div class="modal-desc"> </div>
        <div class="modal-btn">
            <button class="btn-close">Okay</button>
        </div>
    </div>
    <%-- Axios to make HTTP request --%>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="jdbcPage.js"></script>
</body>
</html>
